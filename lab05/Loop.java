import java.util.Scanner;
public class Loop{
  public static void sop(Object printStatement){
    //method that makes it easier to print things out
    System.out.println(printStatement);
  }
  
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);
    
    int courseNum = 0;
    String deptName = "";
    int meetsPerWeek = 0;
    String time = "";
    String instructorName = "";
    int numStudents = 0;
    
    while(true) {
      sop("please enter the course number");
      boolean correct = myScanner.hasNextInt();
      if(!correct){
        myScanner.next();
        sop("thats an incorrect course number!");
      } else {
          courseNum = myScanner.nextInt();
          sop(courseNum);
          break;
      }
    }
    while(true) {
      sop("please enter the department name");
      deptName = myScanner.next();
      sop(deptName);
      break;
    }
    while(true) {
      sop("please enter the # of meetings per week");
      boolean correct = myScanner.hasNextInt();
      if(!correct){
        myScanner.next();
        sop("thats an incorrect # of meetings per week!");
      } else {
          meetsPerWeek = myScanner.nextInt();
          sop(meetsPerWeek);
          break;
      }
    }
    while(true) {
      sop("please enter the time");
      time = myScanner.next();
      sop(time);
      break;
    }
    while(true) {
      sop("please enter the instructor Name");
      instructorName = myScanner.next();
      sop(instructorName);
      break;
    }
    while(true) {
      sop("please enter the # of students");
      boolean correct = myScanner.hasNextInt();
      if(!correct){
        myScanner.next();
        sop("thats an incorrect # of students!");
      } else {
          numStudents = myScanner.nextInt();
          sop(numStudents);
          break;
      }
    }
    
    sop(courseNum + " " +  deptName + " " + meetsPerWeek + " " + time + " " + instructorName + " " + numStudents);
    }
  }