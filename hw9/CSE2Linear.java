import java.util.Scanner;

public class CSE2Linear {
    public static void print(int[] array) {
	for (int i = 0; i < array.length; i++){
	    System.out.println(array[i]);
	}
    }
    public static int binarySearch (int searchElement, int[] array) {
	int pivotPoint = array.length / 2; //this is the position of the pivot within the array
	int pivot = array[pivotPoint]; // this is the value at the pivot point
	int counter = 0;
  while (searchElement != pivot) {
	    if (counter >= array.length) {
		pivotPoint = -1;
		break;
	    }
	    if (searchElement > pivot) {
		    pivotPoint += (array.length - pivotPoint)/2;
		    pivot = array[pivotPoint];
        counter += 1;
	    } else if (searchElement < pivot){
		    pivotPoint -= pivotPoint/2;
		    pivot = array[pivotPoint];
	      counter += 1;
      } 
	}
	return pivotPoint;
    }
    public static int linearSearch(int searchElement, int[] array){
	int foundPosition = -1; //this is the default if the element isnt found
	for (int i = 0; i < array.length; i++){
	    if (array[i] == searchElement) {
		foundPosition = i;
		break;
	    }
	} return foundPosition;
    }
    public static int randomScrambler(int searchElement, int[] array){
	int foundPosition = -1;
	for (int i = 0; i < 1000; i++){
	    int randomNumber = (int) (Math.random() * array.length); //random number is created with each loop
	    if (array[randomNumber] == searchElement){
		foundPosition = randomNumber;
		break; //stops loops cause like already found
	    }
	} return foundPosition;
    }
    public static void main (String[] args) {
	Scanner sc = new Scanner(System.in);
	int[] grades = new int[15];
	System.out.println("Enter 15 ascending ints for final grades in CSE2:");
	for (int i = 0; i < 15; i++) {          
	    do{ 
		if (sc.hasNextInt()) {
		    int input = sc.nextInt();
		    if (input > 100 || //first checked for out of boudns
			input < 0) {
			System.out.println("error out of bounds");
		    } else if (i != 0) {
			if (input != grades[i-1] &&
			    input < grades[i-1] ) { //not greator than hehe
			    System.out.println("error not greater");
			} else{
			    System.out.println("hello");
			    grades[i] = input;
			    break;
			}
		    } else {
			//System.out.println("hello");
			grades[i] = input;
			break;
		    }
		} else {
		    System.out.println("Thats not an int");
		    sc.next();
		}
	    } while(true);
	} print(grades);
	System.out.print("Enter a grade to search for:");
	int searchElement = sc.nextInt();
	System.out.println(binarySearch(searchElement, grades));
	//System.out.println("its done");
    }
}
