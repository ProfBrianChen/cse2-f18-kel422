//CSE 002 Lab04
//Kenny Lin 

import java.lang.Math;

public class CardGenerator {

	public static void sop(String printStatement){
    //method that makes it easier to print things out
    System.out.println(printStatement);
  }
  
  public static void main(String[] args) {
    String suit = "";
    
    double randNum = (int) ( (Math.random() * 52) + 1 ); 
    //we dont want 0 and need 52 to be included 
    sop("randNum " + randNum);
    
    double suitName = randNum / 13;
    sop("suitName " + suitName);  
    
    if (suitName > 3){
      suit = "Spades";
    } else if (suitName > 2) {
      suit = "Hearts";
    } else if (suitName > 1) {
       suit = "Clover";
    } else if (suitName >0){
        suit = "Diamonds";
      } else {
      sop ("error"); }
    sop(suit); //testing to make sure it works 
    
    int cardIdentity = (int) (randNum % 13);
    sop("cardIdentity " + cardIdentity);
    String identity = "";
    switch (cardIdentity){
      case 1: cardIdentity = 1;
        identity = "ace";
        break;
      case 2: cardIdentity = 2;
        identity = "two";
        break;
      case 3: cardIdentity = 3;
        identity = "three";
        break;
      case 4: cardIdentity = 4;
        identity = "four";
        break;
      case 5: cardIdentity = 5;
        identity = "five";
        break;
      case 6: cardIdentity = 6;
        identity = "six";
        break;
      case 7: cardIdentity = 7;
        identity = "seven";
        break;
      case 8: cardIdentity = 8;
        identity = "eight";
        break;
      case 9: cardIdentity = 9;
        identity = "nine";
        break;
      case 10: cardIdentity = 10;
        identity = "two";
        break;
      case 11: cardIdentity = 11;
        identity = "jack";
        break;
      case 12: cardIdentity = 12;
        identity = "queen";
        break;
      case 13: cardIdentity = 13;
        identity = "king";
        break;   
      default: 
        identity = "error";
        break;
    }
    sop ("cardIdentity " + identity);
    sop ("you picked " + identity + " of " + suit);
    }
} 

  