//CSE 002 Lab02
//Kenny Lin

//Calculates time and distance of cycling trips based on user input 

public class Cyclometer {
    //main method 
    public static void main(String[] args) {
   //end main method
  
  //input data (two cycling trips)
  int secsTrip1 = 480; //declares and assigns int var for time of trip 1 in secs
  int secsTrip2 = 3229; //int var time trip 2 
  int countsTrip1 = 1561; //int var # of counts trip 1
  int countsTrip2 = 9037; //int var # counts trip 2
  
  //bike/speed values and output values
  double wheelDiameter = 27.0; //declares and assigns double var for diamter of wheel 
  double PI = 3.14159; //double var pi
  double feetPerMile = 5280; 
  double inchesPerFoot = 12;
  double secondsPerMinute = 60;
  double distanceTrip1, distanceTrip2, totalDistance; 
      //iniates all distance values 
  
  //print trip information 
  System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + 
                     " minutes and had " + countsTrip1 + " counts.");
  System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + 
                     " minutes and had " + countsTrip2 + " counts.");

  //trip calculations 
  distanceTrip1 = countsTrip1 * wheelDiameter * PI;
    //gives distance in inches 
    //each count, wheel rotates wheelDiameter times PI
  distanceTrip1 /= inchesPerFoot * feetPerMile; 
    //converts distance inches to miles 
  distanceTrip2 = countsTrip2 * wheelDiameter * PI 
                  / inchesPerFoot
                  / feetPerMile;
  totalDistance = distanceTrip1 + distanceTrip2; //adds distances together 
      
  //print output of calculations
  System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");
    }
  
} //end class