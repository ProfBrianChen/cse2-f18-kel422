//Kenny Lin 
//CSE 002 HW04

import java.util.Scanner;
import java.lang.Math;

public class CrapsIf {
  public static void sop (Object printStatement) {
    //method that makes it easier to print things out
    System.out.println(printStatement);
  }
  public static void main(String[] args){
    
    int dice1 = 0;
    int dice2 = 0;
    
    //will use this to make sure user input is in correct range
    int checker = 0;
    
    //defining names of call outs for ease in future 
    String SE = "Snake Eyes";
    String AD = "Ace Deuce";
    String EF = "Easy Four";
    String HF = "Hard Four";
    String FF = "Fever Five";
    String ES = "Easy Six";
    String HS = "Hard Six";
    String SO = "Seven Out";
    String EE = "Easy Eight";
    String HE = "Hard Eight";
    String N  = "Nine";
    String ET = "Easy Ten";
    String HT = "Hard Ten";
    String YE = "Yo-Leven";
    String B  = "Boxcars";

    
    Scanner myScanner = new Scanner(System.in); 
    System.out.print("enter 1 for random or 2 for user provided input"); //this gives users choice on what they want to dp
    int userResponse = myScanner.nextInt();
    
    //part one collecting user response
    if (userResponse == 1) { //if they want random, do this
      dice1 = (int) ( (Math.random() * 6) + 1);
      sop("your first dice value is " + dice1);
      
      dice2 = (int) ( (Math.random() * 6) + 1); //sets dice to ramdom integers between 1 and 6
      sop("your second dice value is " + dice2);  
    } else if (userResponse == 2) { //they want to enter manually
      sop("enter integer for dice 1");
      int input1 = (int) myScanner.nextInt();
      if (input1 >= 1 && input1 <= 2){ 
        dice1 = input1;
        sop("your first dice value is " + dice1);
        
        sop("enter integer for dice 2");
        int input2 = (int) myScanner.nextInt();
        if (input2 >= 1 && input2 <= 2){
          dice2 = input2;
          sop("your second dice value is " + dice2);
        } else { //these else statements are to prevent ppl from entering numbers that are not 1 or 2 
          //i know we didnt have to do this but i just did it idk why lmao
        sop("please enter a valid number"); 
        checker += 1;}
      } else {
        sop("please enter a valid number"); 
        checker += 1;}  
    } else {
        sop("please enter a valid number"); 
        checker += 1;}
  //its really bulky but i didnt know how else to do it :( 
  
  //part two naming throws
  if (dice1 == 1 && checker == 0) { //gonna do this one by one for all the pairings using nested ifs
    if(dice2 == 1) {
      sop(SE); 
    } else if(dice2 == 2){
      sop(AD);
    }else if(dice2 == 3){
      sop(EF);
    } else if(dice2 == 4){
      sop(FF);
    } else if(dice2 == 5){
      sop(ES);
    }else if(dice2 == 6){
      sop(SO);
    }  //end of dice 1
  } else if (dice1 == 2) { //gonna do this one by one for all the pairings using nested ifs
    if(dice2 == 1) {
      sop(AD); 
    } else if(dice2 == 2){
      sop(HF);
    }else if(dice2 == 3){
      sop(FF);
    } else if(dice2 == 4){
      sop(ES);
    } else if(dice2 == 5){
      sop(SO);
    }else if(dice2 == 6){
      sop(EE);
    } 
  } else if (dice1 == 3) { //gonna do this one by one for all the pairings using nested ifs
    if(dice2 == 1) {
      sop(EF); 
    } else if(dice2 == 2){
      sop(FF);
    }else if(dice2 == 3){
      sop(HS);
    } else if(dice2 == 4){
      sop(SO);
    } else if(dice2 == 5){
      sop(EE);
    }else if(dice2 == 6){
      sop(N);
    } 
  } else if (dice1 == 4) { //gonna do this one by one for all the pairings using nested ifs
    if(dice2 == 1) {
      sop(FF); 
    } else if(dice2 == 2){
      sop(ES);
    }else if(dice2 == 3){
      sop(SO);
    } else if(dice2 == 4){
      sop(HE);
    } else if(dice2 == 5){
      sop(N);
    }else if(dice2 == 6){
      sop(ET);
    } 
  } else if (dice1 == 5) { //gonna do this one by one for all the pairings using nested ifs
    if(dice2 == 1) {
      sop(ES); 
    } else if(dice2 == 2){
      sop(SO);
    }else if(dice2 == 3){
      sop(EE);
    } else if(dice2 == 4){
      sop(N);
    } else if(dice2 == 5){
      sop(HT);
    }else if(dice2 == 6){
      sop(YE);
    } 
  } else if (dice1 == 6) { //gonna do this one by one for all the pairings using nested ifs
    if(dice2 == 1) {
      sop(SO); 
    } else if(dice2 == 2){
      sop(EE);
    }else if(dice2 == 3){
      sop(N);
    } else if(dice2 == 4){
      sop(ET);
    } else if(dice2 == 5){
      sop(YE);
    }else if(dice2 == 6){
      sop(B);
    } 
  }
  } //end of main method 
} //end of class          
            