//Kenny Lin 
//CSE 002 HW04

import java.util.Scanner;
import java.lang.Math;

public class CrapsSwitch {
  public static void sop (Object printStatement) {
    //method that makes it easier to print things out
    System.out.println(printStatement);
  }
  public static void main(String[] args){

    int dice1 = 0;
    int dice2 = 0;
    
    //will use this to make sure user input is in correct range
    int checker = 0;
    
    //defining names of call outs for ease in future 
    String SE = "Snake Eyes";
    String AD = "Ace Deuce";
    String EF = "Easy Four";
    String HF = "Hard Four";
    String FF = "Fever Five";
    String ES = "Easy Six";
    String HS = "Hard Six";
    String SO = "Seven Out";
    String EE = "Easy Eight";
    String HE = "Hard Eight";
    String N  = "Nine";
    String ET = "Easy Ten";
    String HT = "Hard Ten";
    String YE = "Yo-Leven";
    String B  = "Boxcars";
    
    Scanner myScanner = new Scanner(System.in); 
    sop("enter 1 for random or 2 for user provided input"); //this gives users choice on what they want to dp
    int userResponse = myScanner.nextInt();
    
    //part one collecting user response
    if (userResponse == 1) { //if they want random, do this
      dice1 = (int) ( (Math.random() * 6) + 1);
      sop("your first dice value is " + dice1);
      
      dice2 = (int) ( (Math.random() * 6) + 1); //sets dice to ramdom integers between 1 and 6
      sop("your second dice value is " + dice2);  
    } else if (userResponse == 2) { //they want to enter manually
      sop("enter integer for dice 1");
      int input1 = (int) myScanner.nextInt();
      if (input1 >= 1 && input1 <= 2){ 
        dice1 = input1;
        sop("your first dice value is " + dice1);
        
        sop("enter integer for dice 2");
        int input2 = (int) myScanner.nextInt();
        if (input2 >= 1 && input2 <= 2){
          dice2 = input2;
          sop("your second dice value is " + dice2);
        } else { //these else statements are to prevent ppl from entering numbers that are not 1 or 2 
          //i know we didnt have to do this but i just did it idk why lmao
        sop("please enter a valid number"); 
        checker += 1;}
      } else {
        sop("please enter a valid number"); 
        checker += 1;}  
    } else {
        sop("please enter a valid number"); 
        checker += 1;}
  //its really bulky but i didnt know how else to do it :( 
    
   sop("calculating name of throw");
  //part two naming throws
    if (checker == 0) { //check if the first conditon is a ok
    switch (dice1) { //switch statement for the dice one equal to value 1-6
      case 1: dice1 = 1;
        switch (dice2) { //switch statemnt for dice two value
      case 1: dice2 = 1; // i think i have to use nested switch statements 
        sop(SE);
        break;
      case 2: dice2 = 2; 
        sop(AD);
        break;
      case 3: dice2 = 3;
        sop(EF);
        break;
      case 4: dice2 = 4;
        sop(FF);
        break;
      case 5: dice2 = 5; 
        sop(ES);
        break;
      case 6: dice2 = 6;
        sop(SO);
        break;
    } break;
      case 2: dice1 = 2;
      switch (dice2) {
      case 1: dice2 = 1;
        sop(AD);
        break;
      case 2: dice2 = 2; 
        sop(HF);
        break;
      case 3: dice2 = 3;
        sop(FF);
        break;
      case 4: dice2 = 4;
        sop(ES);
        break;
      case 5: dice2 = 5; 
        sop(SO);
        break;
      case 6: dice2 = 6;
        sop(EE);
        break;
    } break;
      case 3: dice1 = 3;
      switch (dice2) {
      case 1: dice2 = 1;
        sop(EF);
        break;
      case 2: dice2 = 2; 
        sop(HF);
        break;
      case 3: dice2 = 3;
        sop(HS);
        break;
      case 4: dice2 = 4;
        sop(SO);
        break;
      case 5: dice2 = 5; 
        sop(EE);
        break;
      case 6: dice2 = 6;
        sop(N);
        break;
    } break;
      case 4: dice1 = 4;
      switch (dice2) {
      case 1: dice2 = 1;
        sop(FF);
        break;
      case 2: dice2 = 2; 
        sop(ES);
        break;
      case 3: dice2 = 3;
        sop(SO);
        break;
      case 4: dice2 = 4;
        sop(EE);
        break;
      case 5: dice2 = 5; 
        sop(N);
        break;
      case 6: dice2 = 6;
        sop(ET);
        break;
    } break;
      case 5: dice1 = 5; //its the same thing over and over again just different dice values 
      switch (dice2) {
      case 1: dice2 = 1;
        sop(ES);
        break;
      case 2: dice2 = 2; 
        sop(SO);
        break;
      case 3: dice2 = 3;
        sop(EE);
        break;
      case 4: dice2 = 4;
        sop(N);
        break;
      case 5: dice2 = 5; 
        sop(HT);
        break;
      case 6: dice2 = 6;
        sop(YE);
        break;
    } break;
      case 6: dice1 = 6;
      switch (dice2) {
      case 1: dice2 = 1;
        sop(SO);
        break;
      case 2: dice2 = 2; 
        sop(EE);
        break;
      case 3: dice2 = 3;
        sop(N);
        break;
      case 4: dice2 = 4;
        sop(ET);
        break;
      case 5: dice2 = 5; 
        sop(ET);
        break;
      case 6: dice2 = 6;
        sop(B);
        break;
    } break;
  }     
    } else {
      sop("error"); //this is in case none of the values go thrrough for some reason 
    }
} // end of main method 
} //end of class


