//CSE 002 HW 02 pt 2
//Kenny Lin 

import java.util.Scanner; //imports scanner class
public class Pyramid { //creates class

public static void sop(String printStatement){
	//method that makes it easier to print things out
	System.out.println(printStatement);
}

public static void main (String[] args){
	Scanner myScanner = new Scanner(System.in); 
	//declares and initiates scanner class

	sop("please enter the square length");
	double squareDim = myScanner.nextDouble();
	//gets square length from user

	sop("please enter the height of the pyramid");
	double height = myScanner.nextDouble();
	//gets height from user

	//calculating total volume with volume of pyrammid formula 
	double volume = (squareDim * squareDim * height) / 3;
	sop("the total volume of the pyramid is " + volume);
	//prints out volume of pyramid 
}
}
