//CSE 002
//Kenny Lin 

import java.util.Scanner;

public class Convert{

	public static void sop(String printStatement){
    //method that makes it easier to print things out
    System.out.println(printStatement);
  }
  
	public static void main (String[] args){
		Scanner myScanner = new Scanner(System.in);
		//iniatiates and delares object myScanner
		
		sop("please enter the number of acres that were affected by the storm");
		double numAcre = myScanner.nextDouble();
		//number of acres affected by storm

		sop("please enter the amount of rain in inches");
		double rain = myScanner.nextDouble();
		double rainFall = rain / 12; //calculates rain fall in feet based on user date

		double cubAcre =  numAcre * rainFall;//dimension of cubic acre;

		double totalRain = cubAcre / (3.379 * 1000000);
		sop("the total rain is " + totalRain + " cubic miles."); 
		//it doesnt return the exact result in the test but i believe that it is close enough?
	}
}