//Kenny Lin
//CSE 002

//import scanner first
import java.util.Scanner;

//set up class
public class EncryptedX {
  //set up main method 
  public static void main(String[] args){
    //setting up scanner
    Scanner keyboard = new Scanner(System.in);
    System.out.println("print desired size");
    int size = 0;

    //this is the thingy to check if actually int
    while(true){
      boolean isInt = keyboard.hasNextInt();
      if(isInt){ 
        //keyboard int is good
        size = keyboard.nextInt();
        break;
      } else {
        //keyboard int is bad 
        System.out.println("please enter a valid int");
        keyboard.next();
      }
    }

    //generates grid
    for (int i = 0; i < size; i++){
      int counter = 0;
      for (int j = 0; j < size; j++){
        if (j == size - i -1 || j == i){
          System.out.print("  "); //prints blank under special circumstances
        } else {
          System.out.print("x "); //otherwise prints x
        } 
      }//end of nested for loop
      System.out.println();
    }//end of first for loop
  }
}
