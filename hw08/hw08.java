//Kenny Lin
//CSE 002

import java.util.Scanner;
public class hw08{ 
    public static void main(String[] args) { 
	Scanner scan = new Scanner(System.in); 
	//suits club, heart, spade or diamond 
	String[] suitNames={"C","H","S","D"};    
	String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
	String[] cards = new String[52]; 
	String[] hand = new String[5]; 
	int numCards = 5; 
	int again = 1; 
	int index = 51;
	System.out.println();
	//this is the starting array  in order
	System.out.println("Generating deck...");
	for (int i=0; i<52; i++){ 
	    cards[i]=rankNames[i%13]+suitNames[i/13]; 
	    System.out.print(cards[i]+" "); 
	} System.out.println();
	//i commented this out because its redundant to print it out twice
	//printArray(cards); 
	shuffle(cards); 
	printArray(cards); 
	while(again == 1){
	    //If numCards is greater than the number of cards in the deck,
	    //create a new deck of cards.
	    //added if statement to make possible
	    if (numCards> index){
		System.out.println();
		System.out.println("Generating new deck of cards...");
		for (int i=0; i<52; i++){
		    cards[i]=rankNames[i%13]+suitNames[i/13]; 
		    System.out.print(cards[i]+" "); 
		} System.out.println();
		shuffle(cards);
		index = 51;
	    }
	    System.out.println();
	    System.out.println("Generating hand...");
	    hand = getHand(cards,index,numCards); 
	    printArray(hand);
	    index = index - numCards;
	    System.out.println("Enter a 1 if you want another hand drawn"); 
	    again = scan.nextInt();
	}
    }
    //this method is a useful way of printing out the contents of an
    //array in understandable characters
    public static void printArray(String[] list){
	for (int i = 0; i < list.length; i++){
	    System.out.print(list[i] + " ");
	} System.out.println();
    }
    //this method shuffles the array
    //its very cool
    public static String[] shuffle(String[] list){
	//sorry i changed the wording i hope thats okay
	System.out.println("\nShuffling...");
	for (int i = 0; i < 100; i++){
	    int random = (int) (Math.random() * list.length);
	    String original = list[0];
      	    list[0] = list[random];
	    list[random] = original;
	} return list;
    }
    //this method gets the hand
    public static String[] getHand(String[] list, int index, int numCards){
	//the hand is actually a new array that exists to store the contents of the hand
	String[] output = new String[numCards];
	int counter = 0;
	for (int i = index; i > index - numCards; i--){
	    output[counter] = list[i];
	    counter++;
	}

	return output;
    }
}
