//CSE 02 
//HW 02 Welcome Class
//Kenny Lin 

public class WelcomeClass{
  public static void main(String args[]){
    System.out.println("    -----------");
    System.out.println("    | WELCOME |");
    System.out.println("    -----------");
    System.out.println("  ^  ^  ^  ^  ^  ^ ");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-K--E--L--4--2--2->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
  }
}