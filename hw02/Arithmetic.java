//CSE 002 HW02 
//Kenny Lin 

public class Arithmetic {
  
  public static void sop(String printStatement){
    //method that makes it easier to print things out
    System.out.println(printStatement);
  }
  
  public static double taxCalculator(double price){
    //the tax rate
    double paSalesTax = 0.06;
    return ( ((int) ((price * paSalesTax) * 100) / 100.0) ); 
    //force casts tax into an integer and back to remove unnessary decimal points
  }
  
  public static void main(String[] args) {
    
    //Variables
    
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    double pantsTax;
    
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    double shirtTax;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;
    double beltTax;
    
    //total values 
    double totalTax, totalPants, totalShirts, 
           totalBelts, totalPrice, finalTotal; 
    
    //Calculations 
    
    //total cost of each item 
    totalPants = (int) ((numPants * pantsPrice) * 100);
    totalPants /= 100;
    pantsTax = taxCalculator(totalPants); //calls taxCalculator method 
    //i feel like this was more work than just doing it each time lol
    sop("The total price of pants is " + totalPants);
    sop("The total tax of pants is " + pantsTax);
    
    
    totalShirts = (int) ((numShirts * shirtPrice) * 100);
    totalShirts /= 100;
    shirtTax = taxCalculator(totalShirts);
    sop("The total price of shirts is " + totalShirts);
    sop("The total tax of shirts is " + shirtTax);

    
    totalBelts = (int) ((numBelts * beltCost) * 100);
    totalBelts /= 100;
    beltTax = taxCalculator(totalBelts);
    sop("The total price of belts is " + totalBelts);
    sop("The total tax of belts is " + beltTax);
    
    totalPrice = (int) ((totalBelts + totalShirts + totalPants) * 100);
    totalPrice /= 100;
    sop("The total price before tax is " + totalPrice);
    
    totalTax =  (taxCalculator(totalPrice));
    sop("The total amount of tax is " + totalTax);
    
    finalTotal = totalTax + totalPrice;    
    sop("The total after tax is " + finalTotal);
    
  }
}