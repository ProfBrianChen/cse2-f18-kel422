//Kenny Lin
//CSE 001

import java.util.Scanner;

public class Hw05 {

  public static void sop(Object printStatement){
    //method that makes it easier to print things out
    System.out.println(printStatement);
  }

  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);


    //variable for user generated input
    sop("please enter the number of loops");
	double numLoops = 0;
  int counter = 0;
	int numDoublePair = 0;
	int numPairings = 0;
	int numPairCase = 0;
	int numTriples = 0;
	int numQuads = 0;

    //beginning of while loop for loops
    while(true) {
    	boolean isInt = myScanner.hasNextInt();
    	if(isInt){
    		numLoops = myScanner.nextInt();
        counter = (int) numLoops;
    		while(counter > 0){
    			int cardOne = (int) ((((Math.random() * 52) + 1) % 13) + 1);
    			//will result in a random number 1-13
    			//sop(cardOne);
    			int cardTwo   = (int) (((Math.random() * 52) + 1) + 1);
    			int cardThree = (int) (((Math.random() * 52) + 1) + 1);
    			int cardFour  = (int) (((Math.random() * 52) + 1) + 1);
    			int cardFive  = (int) (((Math.random() * 52) + 1) + 1);

    			//makes sure no card is redrawn
    			//if they are not all not equal to each other, the entire hand is redrawn until they are
    			while (true) {
    				if (cardOne != cardTwo && cardTwo != cardThree && cardThree != cardFour && cardFour != cardFive) {
    					cardOne   %= 13;
    					cardTwo   %= 13;
    					cardThree %= 13;
    					cardFour  %= 13;
    					cardFive  %= 13;
    					break; //gets out of infitinte while loop
    				} else {
    					cardTwo   = (int) (((Math.random() * 52) + 1) + 1);
    					cardThree = (int) (((Math.random() * 52) + 1) + 1);
    					cardFour  = (int) (((Math.random() * 52) + 1) + 1);
    					cardFive  = (int) (((Math.random() * 52) + 1) + 1);
    				}
    			}
    			counter -= 1; //this is the iterator for the while loop
    			if (cardOne == cardTwo){
    				numPairings += 1;
    				numPairCase += 1;

    				//all possible values if cardOne is equal to cardtwo
    				if (cardTwo == cardThree) {
    					numTriples += 1;
    					if (cardThree == cardFour){
    						numQuads += 1;
    					}
    				} else if (cardTwo ==  cardFour){
    					numTriples += 1;
    					if (cardThree == cardFive){
    						numQuads += 1;
    					}
    				} else if (cardTwo == cardFive){
    					numTriples += 1;
    				}
    			} else if(cardOne == cardThree){
    				numPairings += 1;
    				numPairCase += 1;
    				if (cardThree == cardFour){
    					numTriples += 1;
    				} else if (cardFour == cardFive){
    					numQuads += 1;
    				}
    			} else if(cardOne == cardFour){
    				numPairings += 1;
    				numPairCase += 1;
    				if (cardFour == cardFive){
    					numTriples += 1;
    				}
    			} else if(cardOne == cardFive){
    				numPairCase += 1;
    			}

    			else if(cardTwo == cardThree) { //starting at card twos now
    				numPairings += 1;
    				numPairCase += 1;
    				if (cardThree == cardFour){
    					numTriples += 1;
    				} else if (cardFour == cardFive){
    					numQuads += 1;
    				}
    			} else if(cardTwo == cardFour){
    				numPairings += 1;
    				numPairCase += 1;
    				if (cardFour == cardFive) {
    					numTriples += 1;
    				}
    			} else if(cardTwo == cardFive){
    				numPairings += 1;
    				numPairCase += 1;
    			}

    			//starting at card three now
    			else if(cardThree == cardFour){
    				numPairings += 1;
    				numPairCase += 1;
    				if (cardFour == cardFive) {
    					numTriples += 1;
    				}
    			} else if(cardThree == cardFive) {
    				numPairings += 1;
    				numPairCase += 1;
    			}

    			//starting at card four now
    			else if(cardFour == cardFive){
    				numPairings += 1;
    				numPairCase += 1;
    			}

    			//this is to differentiate between single and double pairs
    			if (numPairCase > 1) {
    				//sop("hello");
    				numPairCase = 0;
    				numDoublePair += 1;
    				numPairings -= 1;
          }
    		}
        break; //breaks loop if correct
    	} else { //else iterates through until user enters a valid number
    		sop("please enter a valid integer");
    		myScanner.next();
    	}
    }
    //this is to make the number of quads and triples more accurate
    numTriples    -= numQuads;
    numDoublePair -= (numTriples + (numQuads * 2));
    
    //have to use printf to onyl use three decimal places
    System.out.printf("The number of loops: %,.0f%n", numLoops);
    //sop(numQuads/numLoops);
    //someone said to use %n instead of \n
    System.out.printf("The probability of Four-of-a-kind: %.3f%n", (numQuads/numLoops));
    System.out.printf("The probability of Three-of-a-kind: %.3f%n", numTriples/numLoops);
    System.out.printf("The probability of Two-pair: %.3f%n", numPairings/numLoops);
    System.out.printf("The probability of One-pair: %.3f%n", numDoublePair/numLoops);
      }

}
