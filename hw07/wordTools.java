//Kenny Lin 
//CSE 002
//HW 07

import java.util.Scanner;
public class wordTools {
	//takes in and returns user input
	public static String sampleText(){
		Scanner sc = new Scanner(System.in);
		System.out.println("please enter text");
		String s = sc.nextLine();
		System.out.print("you entered ");
		return s;
	}
	//prints out menu and looops until they select valid response
	public static String printMenu(){
		String input = sampleText();
		System.out.println();

		//each line needs an individual print statemnt 
		System.out.println("MENU");
		System.out.println("c - Number of non-whitespace characters");
		System.out.println("w - Number of words");
		System.out.println("f - Find text");
		System.out.println("r - Replace all !'s");
		System.out.println("s - Shorten spaces");
		System.out.println("q - Quit");
		System.out.println("");
		System.out.println("Choose an option:");

		//scanner define 
		Scanner sc = new Scanner(System.in);
		String menuChoice = sc.nextLine();

		//start of loop
		while(true){
			//must use .equals
			//comapring values of objects 
			if (menuChoice.equals("q")){
				return "you are now exiting";
			} if (menuChoice.equals("c")){
				System.out.println(getNumofNonWSCharacters(input));
				System.out.println("please enter another choice or q to quit");
				menuChoice = sc.nextLine();	
			} else if (menuChoice.equals("w")) {
				System.out.println(getNumofWords(input));
				System.out.println("please enter another choice or q to quit");
				menuChoice = sc.nextLine();
			} else if (menuChoice.equals("f")) {
				System.out.println("please enter a search element");
				String searchElement = sc.nextLine();
				System.out.println(findText(searchElement, input));
				System.out.println("please enter another choice or q to quit");
				menuChoice = sc.nextLine();
			} else if (menuChoice.equals("r")) {
				System.out.println(replaceExclamation(input));
				System.out.println("please enter another choice or q to quit");
				menuChoice = sc.nextLine();
			} else if (menuChoice.equals("s")) {
				System.out.println(shortenSpace(input));
				System.out.println("please enter another choice or q to quit");
				menuChoice = sc.nextLine();
			} else {
				System.out.println("invalid input");
				menuChoice = sc.nextLine();
			}
		}
	} //end of printMenu()
	public static String getNumofNonWSCharacters(String input){
		int counter = 0;
		for (int i = 0; i < input.length(); i++){
			//dealing with chars is annoying 
			String currentIndex = input.substring(i, i+1);
			//all whitespace technially 
			if (!  (currentIndex.equals(" ")  ||
					currentIndex.equals("\t") ||
					currentIndex.equals("\r") ||
					currentIndex.equals("\n") ||
					currentIndex.equals("\f")   )) {
				counter += 1;
			}
		} //end of for loop
		return "Number of non whitespace characters: " + counter;
	} //end of getNum method

	//similar to getNumofNonWSCharacters method 
	public static String getNumofWords(String input){
		int counter = 0;
		input += " ";
		for (int i = 0; i < input.length(); i++){
			String currentIndex = input.substring(i,i+1);
			//if the current index is space and the one after is not space 
			if (currentIndex.equals(" ") ){
				if (i != input.length() - 1) {
					if(!(input.substring(i+1,i+2).equals(" "))) {
						//System.out.println(currentIndex);
						counter += 1;
					}
				} else if (i == input.length() -1){
					counter += 1;
				}
			}
		} //end of for loop
		return "Number of words: " + counter;
	} //end of getNumWords method

	//search element first then input
	public static String findText(String searchElement, String input){
		int counter = 0; 
		int numCount = 0;
		//loop for the string parsing 
		for (int i = 0; i < input.length(); i++) {
			String currentIndex = input.substring(i,i+1);
				if (input.substring(i, i+1).equals(searchElement.substring(counter, counter+1))){
					counter += 1;
				}
				if (counter == searchElement.length()){
					numCount += 1; //the actual number of instances of variable  
					counter = 0;
				}	
			} //end of loop
		return "Number of instacnes found is: " + numCount;
	} //end of method 

	public static String replaceExclamation(String input){
		int counter = 0;
		String output = ""; //create new variable to output
		for (int i = 0; i < input.length(); i++){
			String currentIndex = input.substring(i,i+1);
			if (currentIndex.equals("!")){ //if is exclmation replace
				output += ".";
			} else {
				output += currentIndex;
			}
		} //end of loop
		return output;
	} //end of method

	public static String shortenSpace(String input) {
		int counter = 0;
		String output = "";
		//start of loop
		for (int i = 0; i < input.length(); i++){
			String currentIndex = input.substring(i, i+1);
			if (currentIndex.equals(" ") && i != input.length() - 1){
				String secondIndex = input.substring(i+1, i+ 2);
				if (secondIndex.equals(" ")){
					output += ""; //delete space 
				} else if (!secondIndex.equals(" ")){
					output += " "; //or keep
				}
			} else {
				output += currentIndex;
			}
		} return output;
	}

	//main method mainly just to call one method
	public static void main(String[] args){
		System.out.println(printMenu());
	}
}